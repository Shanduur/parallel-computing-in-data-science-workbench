#!/usr/bin/bash
#SBATCH -J benchmark_terminal14
#SBATCH -n 8
#SBATCH -c 8
#SBATCH -p ibm_small
#SBATCH -t 00:30:00

set -e
export LANG=en_US.utf8
export LC_ALL=en_US.utf8
export TIMEFORMAT="%R %S %U"

# Clear the environment from any previously loaded modules
module purge > /dev/null 2>&1

module load compilers/intel
module load mpi/openmpi/3.0.0

echo ""
echo "############# VECLENGTH BENCH #############"
echo ""

for VECLENGTH in 1000000 10000000 200000000; do
    echo "***** omp_dotprod_serial *****"
    echo ""
    echo "===== gcc ====="
    echo ""
    time ./bin/omp_dotprod_serial.gcc.out -v ${VECLENGTH}
    echo ""
    echo "===== intel ====="
    time ./bin/omp_dotprod_serial.intel.out -v ${VECLENGTH}
    echo ""
    echo "***** omp_dotprod_openmp *****"
    echo ""
    echo "===== gcc ====="
    time ./bin/omp_dotprod_openmp.gcc.out -v ${VECLENGTH} -t 8
    echo ""
    echo "===== intel ====="
    time ./bin/omp_dotprod_openmp.intel.out -v ${VECLENGTH} -t 8
    echo ""
    echo "***** omp_dotprod_hybrid *****"
    echo ""
    echo "===== mpicc ====="
    time mpirun -np 2 ./bin/omp_dotprod_hybrid.mpicc.out -v ${VECLENGTH} -t 4
    echo ""
    echo "***** omp_dotprod_mpi *****"
    echo ""
    echo "===== mpicc ====="
    time mpirun -np 8 ./bin/omp_dotprod_mpi.mpicc.out -v ${VECLENGTH}
    echo ""
    echo "-*-*- END -*-*-"
done

echo ""
echo "############# MULTICORE BENCH #############"
echo ""

VECLENGTH=200000000
for CORES in 2 4 8; do
    echo "***** omp_dotprod_openmp *****"
    echo ""
    echo "===== gcc ====="
    time ./bin/omp_dotprod_openmp.gcc.out -v ${VECLENGTH} -t ${CORES}
    echo ""
    echo "===== intel ====="
    time ./bin/omp_dotprod_openmp.intel.out -v ${VECLENGTH} -t ${CORES}
    echo ""
    echo "***** omp_dotprod_mpi *****"
    echo ""
    echo "===== mpicc ====="
    time mpirun -np ${CORES} ./bin/omp_dotprod_mpi.mpicc.out -v ${VECLENGTH}
    echo ""
    echo "-*-*- END -*-*-"
done

echo ""
echo "############# HYBRID BENCH #############"
echo ""

for CORES in 2 4 8; do
    for PROCESSES in 2 4 8; do
        echo "***** omp_dotprod_hybrid *****"
        echo ""
        echo "===== mpicc ====="
        time mpirun -np ${PROCESSES} ./bin/omp_dotprod_hybrid.mpicc.out -v ${VECLENGTH} -t ${CORES}
        echo ""
    done
done