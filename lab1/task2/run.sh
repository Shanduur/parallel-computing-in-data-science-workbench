#!/usr/bin/env bash
# compile task
#SBATCH -J compile_terminal14
#SBATCH -N 1
#SBATCH --exclusive
#SBATCH -t 1-00:00:00

set -e
export LANG=en_US.utf8
export LC_ALL=en_US.utf8

# Compilation in SLURM

# Clear the environment from any previously loaded modules
module purge > /dev/null 2>&1

module load compilers/intel
module load mpi/openmpi/3.0.0

rm -rf ./bin/
mkdir -p ./bin/

gcc -fopenmp -DVECLEN=1e8 ../src/omp_dotprod_serial.c -o ./bin/omp_dotprod_serial.gcc.out
icc -openmp -DVECLEN=1e8 ../src/omp_dotprod_serial.c -o ./bin/omp_dotprod_serial.intel.out

gcc -fopenmp -DNUMTHREADS=8 -DVECLEN=1e8 ../src/omp_dotprod_openmp.c -o ./bin/omp_dotprod_openmp.gcc.out
icc -openmp -DNUMTHREADS=8 -DVECLEN=1e8 ../src/omp_dotprod_openmp.c -o ./bin/omp_dotprod_openmp.intel.out

mpicc -fopenmp -DNUMTHREADS=8 -DVECLEN=1e8  ../src/omp_dotprod_hybrid.c -o ./bin/omp_dotprod_hybrid.mpicc.out
mpicc -DVECLEN=1e8 ../src/omp_dotprod_mpi.c -o ./bin/omp_dotprod_mpi.mpicc.out
