#!/usr/bin/bash

module purge > /dev/null 2>&1

module load mpi/openmpi/3.0.0

rm -rf ./bin/
mkdir -p ./bin/

gcc -U__STRICT_ANSI__ -std=c11 ./src/pi_serial.c -o ./bin/pi_serial.gcc.out

gcc -U__STRICT_ANSI__ -std=c11 -fopenmp ./src/pi_omp_reduction.c -o ./bin/pi_omp_reduction.gcc.out

for t in 2 4 8; do
    gcc -U__STRICT_ANSI__ -std=c11 -fopenmp -DNUM_THREADS=${t} ./src/pi_omp.c -o ./bin/pi_omp.${t}.gcc.out
done