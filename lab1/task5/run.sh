#!/usr/bin/bash
#SBATCH -J pi_terminal14
#SBATCH -n 1
#SBATCH -c 8
#SBATCH -p ibm_small
#SBATCH -t 00:30:00

set -e
export LANG=en_US.utf8
export LC_ALL=en_US.utf8
export TIMEFORMAT="%R %S %U"

# Clear the environment from any previously loaded modules
module purge > /dev/null 2>&1

module load mpi/openmpi/3.0.0

echo "serial"
time ./bin/pi_serial.gcc.out

for t in 2 4 8; do
    export OMP_NUM_THREADS=${t}
    echo "${t}"
    echo "pi_omp"
    time ./bin/pi_omp.${t}.gcc.out
    # echo "pi_omp_reduction"
    # time ./bin/pi_omp_reduction.gcc.out
done