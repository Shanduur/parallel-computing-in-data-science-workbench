#!/usr/bin/env bash

set -e

export LANG=en_US.utf8
export LC_ALL=en_US.utf8

module load compilers/intel
module load mpi/openmpi/3.0.0

rm -rf ./bin/
mkdir -p ./bin/

gcc -fopenmp ../src/omp_dotprod_serial.c -o ./bin/omp_dotprod_serial.gcc.out
icc -openmp ../src/omp_dotprod_serial.c -o ./bin/omp_dotprod_serial.intel.out

gcc -fopenmp -DNUMTHREADS=8 ../src/omp_dotprod_openmp.c -o ./bin/omp_dotprod_openmp.gcc.out
icc -openmp -DNUMTHREADS=8 ../src/omp_dotprod_openmp.c -o ./bin/omp_dotprod_openmp.intel.out

mpicc -fopenmp -DNUMTHREADS=8 ../src/omp_dotprod_hybrid.c -o ./bin/omp_dotprod_hybrid.mpicc.out
mpicc ../src/omp_dotprod_mpi.c -o ./bin/omp_dotprod_mpi.mpicc.out
