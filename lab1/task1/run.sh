#!/usr/bin/env bash

set -e

export LANG=en_US.utf8
export LC_ALL=en_US.utf8

rm -rf ./logs/
mkdir -p ./logs

echo omp_dotprod_serial
echo gcc
time ./bin/omp_dotprod_serial.gcc.out 2&>1 | tee ./logs/omp_dotprod_serial.gcc.time
echo intel
time ./bin/omp_dotprod_serial.intel.out 2&>1 | tee ./logs/omp_dotprod_serial.intel.time

echo omp_dotprod_openmp
echo gcc
time ./bin/omp_dotprod_openmp.gcc.out 2&>1 | tee ./logs/omp_dotprod_openmp.gcc.time
echo intel
time ./bin/omp_dotprod_openmp.intel.out 2&>1 | tee ./logs/omp_dotprod_openmp.intel.time

echo omp_dotprod_hybrid
echo mpirun
time mpirun -np 8 ./bin/omp_dotprod_hybrid.mpicc.out 2&>1 | tee ./logs/omp_dotprod_hybrid.mpicc.time

echo omp_dotprod_mpi
echo mpirun
time mpirun -np 8 ./bin/omp_dotprod_mpi.mpicc.out 2&>1 | tee ./logs/omp_dotprod_mpi.mpicc.time
