#!/usr/bin/env bash

set -e

export LANG=en_US.utf8
export LC_ALL=en_US.utf8

CFLAGS="${CFLAGS} -O3"

module load compilers/intel
module load mpi/openmpi/3.0.0

rm -rf ./bin/
mkdir -p ./bin/

gcc ${CFLAGS} -fopenmp \
    ../src/omp_dotprod_serial.c \
    -o ./bin/omp_dotprod_serial.gcc.out

icc ${CFLAGS} -openmp \
    ../src/omp_dotprod_serial.c \
    -o ./bin/omp_dotprod_serial.intel.out

gcc ${CFLAGS} -fopenmp \
    ../src/omp_dotprod_openmp.c \
    -o ./bin/omp_dotprod_openmp.gcc.out

icc ${CFLAGS} -openmp \
    ../src/omp_dotprod_openmp.c \
    -o ./bin/omp_dotprod_openmp.intel.out

mpicc ${CFLAGS} -fopenmp \
    ../src/omp_dotprod_hybrid.c \
    -o ./bin/omp_dotprod_hybrid.mpicc.out

mpicc ${CFLAGS} \
    ../src/omp_dotprod_mpi.c \
    -o ./bin/omp_dotprod_mpi.mpicc.out
