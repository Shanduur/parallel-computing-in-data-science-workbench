maxIterations = 500;
gridSize = 1000;
xlim = [-0.748766713922161, -0.748766707771757];
ylim = [ 0.123640844894862, 0.123640851045266];

% Setup
t = tic();
x = linspace( xlim(1), xlim(2), gridSize );
y = linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );
z0 = xGrid + 1i*yGrid;
count1 = ones( size(z0) );

% Calculate
z = z0;
for n = 0:maxIterations
    z = z.*z + z0;
    inside = abs( z )<=2; count1 = count1 + inside;
end
count1 = log( count1 );
% Show
cpuTime = toc( t )
save cpuTime cpuTime; save count1 count1;

% Setup 2
t = tic();
x = gpuArray.linspace( xlim(1), xlim(2), gridSize );
y = gpuArray.linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );
z0 = complex( xGrid, yGrid );
count2 = ones( size(z0), 'gpuArray' );

% Calculate
z = z0;
for n = 0:maxIterations
    z = z.*z + z0;
    inside = abs( z )<=2; count2 = count2 + inside;
end
count2 = log( count2 );

% Show
count2 = gather( count2 );

% Fetch the data back from the GPU
naiveGPUTime = toc( t )
save naiveGPUTime naiveGPUTime;
save count2 count2;

% Setup 3
t = tic();
x = gpuArray.linspace( xlim(1), xlim(2), gridSize );
y = gpuArray.linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );

% Calculate
count3 = arrayfun( @pctdemo_processMandelbrotElement, xGrid, yGrid, maxIterations );

% Show
count3 = gather( count3 );

% Fetch the data back from the GPU
gpuArrayfunTime = toc( t )
save gpuArrayfunTime gpuArrayfunTime; save count3 count3;
