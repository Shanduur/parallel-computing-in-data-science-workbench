#!/usr/bin/bash
#SBATCH -J matlab
#SBATCH -n 1
#SBATCH -c 8
#SBATCH -p old_gpu
#SBATCH -t 00:30:00

set -e
export LANG=en_US.utf8
export LC_ALL=en_US.utf8
export TIMEFORMAT="%R %S %U"

# srun --pty -n 1 -p old_gpu bash

# Clear the environment from any previously loaded modules
module purge > /dev/null 2>&1

module load matlab/R2017a

sudo matlab -nodesktop -nosplash -nodisplay -r "task2a;exit"
