function benchPlot()

% Copyright 2011 The MathWorks, Inc.

b = load('benchData.mat');
fh = figure('Units', 'pixels', 'Position', [100 100 800 500], 'PaperPositionMode', 'auto');
movegui(fh, 'center');

% Linear Scale
ax1 = axes('Units', 'normalized', 'Position', [0.1 0.1 0.39, 0.8], 'FontSize', 12);
h1 = plot(b.gridSizes, b.results(:, 1), '.-', ...
    b.gridSizes, b.results(:, 2), '.-');
set(ax1, 'XLim', [0, 2200], 'XTick', 0:500:2000);
set(h1, 'MarkerSize', 20, 'LineWidth', 2);

xlabel('Grid Size (N)', 'FontSize', 12);
ylabel('Time for 50 iterations (sec)', 'FontSize', 12);
title('Linear Scale', 'FontSize', 12);
legend('CPU', 'GPU', 'Location', 'NorthWest');

% Log Scale
ax2 = axes('Units', 'normalized', 'Position', [0.51 0.1 0.39, 0.8], 'FontSize', 12);
h2 = semilogy(b.gridSizes, b.results(:, 1), '.-', ...
    b.gridSizes, b.results(:, 2), '.-');
set(ax2, 'XLim', [0, 2200], 'XTick', 0:500:2000, 'YAxisLocation', 'right');
set(h2, 'MarkerSize', 20, 'LineWidth', 2);

xlabel('Grid Size (N)', 'FontSize', 12);
ylabel('Time for 50 iterations (sec)', 'FontSize', 12);
title('Log Scale', 'FontSize', 12);

% Uncomment the following line to save plot as a JPEG
% print(fh, '-djpeg', '-r300', 'benchmark.jpg');