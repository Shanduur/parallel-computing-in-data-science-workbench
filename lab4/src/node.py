from array import array
from email.mime import base
import json
import socket
import glob
import os.path as path
import numpy as np
from datetime import datetime

from timeit import TimeIt


class Node():
    def __init__(self, comm) -> None:
        self.comm = comm
        self.id = self.comm.Get_rank()
        self.size = self.comm.Get_size()
        self.hostname = socket.gethostname()
        self.num_tasks = 0

        if self.id == 0:
            self.name = 'main'
            with open('config.json') as f:
                self.config = json.load(f)
                self.config['files'] = glob.glob(self.config['data_path'])
                self.num_tasks = len(self.config['files'])
            self.timer = TimeIt(self.size)
        else:
            self.name = 'compute'

        self.num_tasks = self.comm.bcast(self.num_tasks)

        print(f'{self} | init ok | tasks: {self.num_tasks}')

    def __str__(self) -> str:
        return f'{datetime.now().time()} | {self.name} ({self.id}) @ {self.hostname}'

    def __del__(self) -> None:
        print(f'{self} | shutting down')

    def _multiply(self, x: tuple) -> int:
        try:
            return x[0] * x[1]
        except Exception:
            return -1

    def _p(self, x) -> None:
        if self.id == 0:
            print(x)

    def Run(self) -> None:
        for i in range(0, self.num_tasks):
            if self.id == 0:
                self.timer.Tik()
                self._p(f'{self} | {self.config["files"][i]}')
            array = np.empty([0, 0], dtype=np.ubyte)
            if self.id == 0:
                array = self.Load(i)
                self.timer.SetSize(array.shape[0])

            shape = self.comm.bcast(array.shape)
            self._p(f'{self} | bcast | {shape}')

            buffer = np.empty(shape[0]//self.size, dtype=np.ubyte)
            self._p(f'{self} | buffer | {buffer.shape}')
            self.comm.Scatter(array, buffer)

            out = self.Process(buffer)

            buffer = np.empty(array.shape, dtype=np.ubyte)
            self._p(f'{self} | out buffer | {buffer.shape}')

            self.comm.Gather(out, buffer)
            self._p(f'{self} | out | {buffer.shape}')

            if self.id == 0:
                array = self.Save(i, buffer)
            
            if self.id == 0:
                self.timer.Tok()

        print(f'{self} | work done')


    def Load(self, id: int) -> np.ndarray:
        assert self.id == 0
        name = self.config['files'][id]
        basename = path.basename(name)

        self._p(f'{self} | {basename}')
        self._p(f'{self} | starting loading file')

        f = np.fromfile(name, dtype=np.ubyte)
        self._p(f'{self} | done | {f.shape}')

        f = f.reshape(f.shape[0])
        self._p(f'{self} | reshape | {f.shape}')

        return f

    def Save(self, id: int, arr: np.ndarray):
        assert self.id == 0
        shape = self.GetShape(id)
        arr = arr.reshape(shape)
        name = path.basename(self.config['files'][id]) + ".out"
        self._p(f'{self} | shape:{arr.shape} | saving output to: {name}')
        arr.tofile(name)

    def GetShape(self, id) -> tuple:
        assert self.id == 0
        name = self.config['files'][id]
        basename = path.basename(name)
        root = path.splitext(basename)[0]
        shape = root.replace('infile', '').split('_')
        try:
            shape = (int(shape[0]),int(shape[1]))
            return shape
        except Exception as e:
            self._p(f'{self} | ERROR | {e}')


    def Process(self, arr: np.ndarray, contrast_factor: float = 2.0, brightness: int = 0) -> np.ndarray:
        print(f'{self} | processing started')
        contrast_factor = float(contrast_factor)
        floated = arr.astype(np.float)
        adjusted = contrast_factor * (floated - 128.0) + 128.0 + brightness
        clipped = np.clip(adjusted, 0, 255).astype(np.ubyte)
        print(f'{self} | processing done')
        return clipped
