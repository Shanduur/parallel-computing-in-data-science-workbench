import time
import pandas as pd
import os
import os.path as path


class TimeIt():
    def __init__(self, workers: int) -> None:
        self._c_workers = workers
        self._c_size = 0
        self._laps = []
        self._dumpdir = 'timing'

    def Tik(self) -> None:
        self._lap = time.time()

    def SetSize(self, size: int) -> None:
        self._c_size = size

    def Tok(self) -> None:
        self._laps.append([self._c_workers, self._c_size, time.time() - self._lap])

    def __del__(self) -> None:
        df = pd.DataFrame(self._laps)
        os.makedirs(self._dumpdir, exist_ok=True)
        df.to_csv(path.join(self._dumpdir, f'timeit-{self._c_workers}.csv'), header=False, index=False)
        print(df)
