from mpi4py import MPI

from node import Node


def main():
    n = Node(MPI.COMM_WORLD)
    n.Run()

if __name__ == '__main__':
    main()
