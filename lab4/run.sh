#!/usr/bin/bash
#SBATCH --job-name=lab4
#SBATCH --ntasks=64
#SBATCH --partition=ibm_small
#SBATCH --time=02:00:00

set -e

export LANG=en_US.utf8
export LC_ALL=en_US.utf8

PROCESSES=(2 4 6 8 10 12 15 16 18 20 24 30 36 40 48 50 60 64)

source ./venv38/bin/activate

module purge > /dev/null 2>&1
module load mpi/openmpi/3.0.0

for i in ${PROCESSES[@]}
do
    echo ""
    echo ""
    echo ""
    echo $i
    echo ""
    echo ""
    echo ""
    mpiexec -n ${i} python3 ./src/main.py
done
