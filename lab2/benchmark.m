function [a, time] = benchmark(A, max_iters)
    tic
    parfor i = 1:max_iters
        a(i) = max(abs(eig(rand(A))));
    end
    time = toc;
end