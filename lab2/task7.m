D = 2;
dx = 1;
dt = 1e-3;

X = (-10:dx:10);
T = ( 0:dt:20);
n_T = length(T);

u_0 = normpdf(X, 2, 3);
U = zeros(length(T), length(X));
U(1,:) = u_0;

tic
for t_ = T(2 : length(T))
    t = find(T==t_);
    for x_ = X(2 : length(X)-1)
        x = find(X==x_);
        U(t, x) = U(t-1, x) + D*dt/dx^2 * (U(t-1, x-1) + U(t-1, x+1) - 2*U(t-1, x));
    end
end
toc

figure(1)
hold on
for t = 1:round(0.1*n_T):n_T
plot(X, U(t,:))
end
legend("t = " + string((0:round(0.1*n_T):n_T)/1000))