workers = 0;

results = zeros(1, 4);

row = 0;
for i = 1:7
    workers = 2^i;
    parpool(workers);
    for iters = 100:50:200
        row = row + 1;
        disp(row)
        
        [a, time] = benchmark(50, iters);

        results(row, 1) = workers;
        results(row, 2) = iters;
        results(row, 3) = time;
    end
    writematrix(results, 'results-t5-a.csv');
    delete(gcp);
end

writematrix(results, 'results-t5.csv');