clear all;
close all;
clc;

p = gcp;

results = zeros(1, 4);
row = 0;
for A = 50:50:500
    for n = 100:100:1000
        disp(row);
        row = row + 1;
        results(row, 1) = A;
        results(row, 2) = n;
       
        a = zeros(n);
        tic
        for i = 1:n      
            a(i) = max(abs(eig(rand(A))));  
        end  
        fortime = toc;
        
        results(row, 3) = fortime;
        
        clear a
        a = zeros(n);
        tic
        parfor i = 1:n      
            a(i) = max(abs(eig(rand(A))));  
        end  
        parfortime = toc;
        
        results(row, 4) = parfortime;
    end
end
writematrix(results, 'results.csv');