USER=terminal14
ADDRESS="157.158.89.130"

while true
do
    rsync \
        -av \
        -e ssh \
        --exclude='.git/' \
        --exclude='.DS_Store' \
        $(pwd) \
        ${USER}@${ADDRESS}:/home/${USER} > /dev/null
    
    sleep 5
done