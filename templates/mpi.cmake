cmake_minimum_required(VERSION 2.8)

find_package(MPI)
include_directories(SYSTEM ${MPI_INCLUDE_PATH})
target_link_libraries(target ${MPI_C_LIBRARIES})
